<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return "hello world";
});

Route::post('/generate-pdf', 'LatexController@generatePDF');
Route::post('/fqz/upload', 'UploadController@get');
Route::get('/telegram/test', 'TelegramBotController@test');
