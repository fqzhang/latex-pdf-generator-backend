<?php
/**
 * Created by PhpStorm.
 * User: hang
 * Date: 9/4/2018
 * Time: 11:45 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class UploadController extends Controller
{
    function get(Request $request){
        ini_set('memory_limit', '-1');
        $hasFile = $request->hasFile('file');
        if($hasFile){ #文件存在
            $file = $request->file('file');//获取文件
            $path = public_path() . '/uploads/'; //放在这个目录
            $fileExtension =  $file->clientExtension();
            $timestramp = round(microtime(true) * 1000);
            $fileName = $timestramp . '.' . $fileExtension ; //文件名以当前的timpstamp命名以免重复
            $file->move($path,$fileName);
			$issueNumber = $this->getIssueNumber(); //获取期数
$latex_data = <<<'EOD'
\documentclass[UTF8,a4paper,15pt]{ctexart} 

\usepackage{textcomp}
\usepackage[doublespacing]{setspace}
\usepackage{amsmath,amssymb}
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{lastpage}
\usepackage{listings}
\usepackage{xcolor}
\usepackage[unicode]{hyperref}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{appendix}
\usepackage[final]{pdfpages}
\usepackage{titlesec}
\usepackage{hyperref}
%\usepackage{geometry}

\usepackage{type1cm}

\setCJKfamilyfont{li}{LiSu}
\newcommand{\song}{\CJKfamily{song}}%宋体
\newcommand{\fs}{\CJKfamily{fs}}%仿宋体
\newcommand{\kai}{\CJKfamily{kai}}%楷体
\newcommand{\hei}{\CJKfamily{hei}}%黑体
\newcommand{\li}{\CJKfamily{li}}%隶书

\newcommand{\yihao}{\fontsize{26pt}{36pt}\selectfont}%一号，1.4倍行距
\newcommand{\erhao}{\fontsize{22pt}{28pt}\selectfont}%二号，1.25倍行距
\newcommand{\xiaoer}{\fontsize{18pt}{18pt}\selectfont}%小二，单倍行距
\newcommand{\sanhao}{\fontsize{16pt}{16pt}\selectfont}%三号，1倍行距
\newcommand{\xiaosan}{\fontsize{15pt}{22pt}\selectfont}%小三,1.5倍行距
\newcommand{\sihao}{\fontsize{14pt}{21pt}\selectfont}%四号，1.5倍行距
\newcommand{\banxiaosi}{\fontsize{13pt}{19.5pt}\selectfont}%半小四，1.5倍行距
\newcommand{\xiaosi}{\fontsize{12pt}{18pt}\selectfont}%小四，1.5倍行距
\newcommand{\dawuhao}{\fontsize{11pt}{11pt}\selectfont}%大五号，单倍行距
\newcommand{\wuhao}{\fontsize{10.5pt}{15.75pt}\selectfont}%五号，单倍行距
\newcommand{\xiaowu}{\fontsize{9pt}{13.5pt}\selectfont}%小五，1.5倍行距
%\usepackage[bf]{package}
%\geometry{a4paper,scale=0.8}
\hypersetup{hidelinks}

\setcounter{secnumdepth}{0}
\begin{document}
\lhead{翻墙者杂志} 
\chead{明日头条}
EOD;

$latex_data .= "\\rhead{第".$issueNumber."期} \n";
$latex_data .= <<<'EOD'
    \begin{center}
        \li\yihao 翻墙者
        \\\li\xiaoer 明日头条
    \end{center}
    \begin{center}
        \textbf{使用说明：}
        \begin{itemize}
            \item 本文中可通过点击新闻全文跳转至新闻页面。
            \item 本文由《翻墙者》杂志分部“明日头条”编写
            \item 联系我们可以通过如下方式：fqzofficial@protonmail.com
			\item 欢迎转载本文档至墙内
        \end{itemize}		
    \end{center}
    \tableofcontents
    \newpage
EOD;

            if($fileExtension == 'docx'){
                $data = $this->read_docx($fileName);
                #return response($data);
                $lines = preg_split("/((\r?\n)|(\r\n?))/", $data);
                $hasArticleCompleted = false;
                $title = '';
                $link = '';
                $footnote = '';
                $content = '';
                $previousLine = '';
                for($i = 0; $i< count($lines); $i++){
                    // 一行一行遍历
                    $line = $this->formatLatexStr($lines[$i]);
                    if($i == 0){
                        $previousLine = $line;
                    } else {
						$previousLine = $this->formatLatexStr($lines[$i-1]);
					}
                    if(empty($title)){
                        $title = $line;
						$title = str_replace(" ", "\ ", $title);
                    }elseif(empty($content)){
                        $content .= $line;
                    } else {
                        if (stripos($line, 'http') !== false) {
                            $index = stripos($line, 'http');
                            if ($index === 0) { // 链接在新一行
                                $link = $line;
                                $footnote = $previousLine;
                                $content = str_replace("\\\\\\indent{". $previousLine . "}","",$content);
                            } else { //脚注跟链接在同一行
								//不知道为什么脚注跟链接中间的空格有时候没了，然后导致最后的一个字母乱码
								//解决方案就是在http前加一个空格
								$line = str_replace("http"," http",$line);
                                $footnote = trim(substr($line,0,$index));
                                $link = substr($line,$index);
                            }
                            // 结束处理一篇文章
                            $latex_data .= "\\subsection{" . $title . "} \n";
                            $latex_data .= "\\href{" . $link . "}{" . $content . "} \n";
                            $latex_data .= "\\footnote{" . $footnote . "} \n";
                            //重置参数
                            $title = '';
                            $link = '';
                            $footnote = '';
                            $content = '';
                            $previousLine = '';
                        } else {
                            $content .= "\n\\\\\\indent{". $line . "}";
                        }
                    }
                }
                $latex_data .= "\n\\end{document}";
				#return response($data);
				#return response($latex_data);
                $tex_filename = $timestramp . '.tex';
                $pdf_filename = $timestramp . '.pdf';
                $f = fopen($tex_filename, 'wb+');
                fwrite($f, $latex_data);
                fclose($f);
                #$output1 = shell_exec("pdflatex $tex_filename");
                #$output2 = shell_exec("pdflatex $tex_filename");
                $output1 = shell_exec("/usr/bin/xelatex $tex_filename");
                $output2 = shell_exec("/usr/bin/xelatex $tex_filename");
                return response()->json([$pdf_filename,$tex_filename], 200);
            } elseif($fileExtension == 'txt'){

            }
        } else {
            return response()->json([],401);
        }
    }
    private function read_docx($fileName){

        $striped_content = '';
        $content = '';

        $zip = zip_open("./uploads/$fileName");

        if (!$zip || is_numeric($zip)) return false;

        while ($zip_entry = zip_read($zip)) {

            if (zip_entry_open($zip, $zip_entry) == FALSE) continue;

            if (zip_entry_name($zip_entry) != "word/document.xml") continue;

            $content .= zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));

            zip_entry_close($zip_entry);
        }// end while

        zip_close($zip);

		#$content = str_replace('</w:r></w:p></w:tc><w:tc>', " ", $content);
        #$content = str_replace('</w:t></w:r><w:r><w:t>', "", $content);
		
		#$content = str_replace('</w:t>', "", $content);
		#$content = str_replace('<w:t>', "", $content);
		#$content = str_replace('</w:r>', "", $content);
		#$content = str_replace('<w:r>', "", $content);
		#$content = str_replace('</w:pPr><w:r>', "\r\n", $content);
		#$content = str_replace('</w:rPr><w:t>', "\r\n", $content);
		#$content = str_replace('</w:r></w:p>', "\r\n", $content);
		#$content = str_replace('</w:rPr><w:lastRenderedPageBreak/><w:t>', "\r\n", $content);
        #$content = str_replace('</w:p>', "\r\n", $content);
		$content = str_replace('<w:p ', "\r\n<w:p ", $content);
        $striped_content = strip_tags($content);
        $striped_content = $this->removeEmptyLines($striped_content);
        return $striped_content;
        #return $content;
    }
    function removeEmptyLines($string){
        return preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $string);
    }
    public function formatLatexStr($str){
		$str = trim($str);
        $str = str_replace("\\", "\\\\", $str);
        $str = str_replace('#', '\#', $str);
        $str = str_replace('$', '\$', $str);
        $str = str_replace('%', '\%', $str);
        $str = str_replace('&', '\&', $str);
        $str = str_replace('_', '\_', $str);
        $str = str_replace('^', '\^', $str);
        $str = str_replace("{", "\{", $str);
        $str = str_replace("}", "\}", $str);
		$str = trim(trim($str,"　")); //过滤中文全角空格
		#$str = str_replace(" ", "\ ", $str);
        return $str;
    }
	public function getIssueNumber(){
		//使用天数来计算期数
		$currentTime = time();
		$fromTime = strtotime("2018-09-01");
		$datediff = $currentTime - $fromTime;
		
		$days = round($datediff / (60 * 60 * 24));
		$numbers=array(
			'1'    =>    '一',
			'2'    =>    '二',
			'3'    =>    '三',
			'4'    =>    '四',
			'5'    =>    '五',
			'6'    =>    '六',
			'7'    =>    '七',
			'8'    =>    '八',
			'9'    =>    '九',
			'10'    =>    '十',
		);
		if($days>10){
			//十位数
			$tenDigit = (int)($days/10);
			//余数
			$remainder = $days%10;
			//如果是 10 < x <20, 那么就不要输出 '一'.  ex. 16 => '十六', not ‘一十六'
			return $tenDigit > 1 ? $numbers[$tenDigit] . $numbers[10] . $numbers[$remainder] : '' . $numbers[10] . $numbers[$remainder];
		}else{
			return $numbers[$days];
		}
	}
}
