<?php
/**
 * Created by PhpStorm.
 * User: hang
 * Date: 9/4/2018
 * Time: 11:45 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class LatexController extends Controller
{
    public function formatLatexStr($str){
        $str = str_replace("\\", "\\\\", $str);
        $str = str_replace('#', '\#', $str);
        $str = str_replace('$', '\$', $str);
        $str = str_replace('%', '\%', $str);
        $str = str_replace('&', '\&', $str);
        $str = str_replace('_', '\_', $str);
        $str = str_replace('^', '\^', $str);
        $str = str_replace("{", "\{", $str);
        $str = str_replace("}", "\}", $str);
        return $str;
    }
    public function generatePDF(Request $request){
        $data = $request->all();
        $left_head_title = $data['left_head_title'];
        $middle_head_title = $data['middle_head_title'];
        $right_head_title = $data['right_head_title'];

        $latex_data = <<<'EOD'
\documentclass[UTF8,a4paper,15pt]{ctexart} 

\usepackage{textcomp}
\usepackage[doublespacing]{setspace}
\usepackage{amsmath,amssymb}
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{lastpage}
\usepackage{listings}
\usepackage{xcolor}
\usepackage[unicode]{hyperref}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{appendix}
\usepackage[final]{pdfpages}
\usepackage{titlesec}
\usepackage{hyperref}
%\usepackage{geometry}

\usepackage{type1cm}
\newcommand{\song}{\CJKfamily{song}}%宋体
\newcommand{\fs}{\CJKfamily{fs}}%仿宋体
\newcommand{\kai}{\CJKfamily{kai}}%楷体
\newcommand{\hei}{\CJKfamily{hei}}%黑体

\newcommand{\yihao}{\fontsize{26pt}{36pt}\selectfont}%一号，1.4倍行距
\newcommand{\erhao}{\fontsize{22pt}{28pt}\selectfont}%二号，1.25倍行距
\newcommand{\xiaoer}{\fontsize{18pt}{18pt}\selectfont}%小二，单倍行距
\newcommand{\sanhao}{\fontsize{16pt}{16pt}\selectfont}%三号，1倍行距
\newcommand{\xiaosan}{\fontsize{15pt}{22pt}\selectfont}%小三,1.5倍行距
\newcommand{\sihao}{\fontsize{14pt}{21pt}\selectfont}%四号，1.5倍行距
\newcommand{\banxiaosi}{\fontsize{13pt}{19.5pt}\selectfont}%半小四，1.5倍行距
\newcommand{\xiaosi}{\fontsize{12pt}{18pt}\selectfont}%小四，1.5倍行距
\newcommand{\dawuhao}{\fontsize{11pt}{11pt}\selectfont}%大五号，单倍行距
\newcommand{\wuhao}{\fontsize{10.5pt}{15.75pt}\selectfont}%五号，单倍行距
\newcommand{\xiaowu}{\fontsize{9pt}{13.5pt}\selectfont}%小五，1.5倍行距
\newcommand{\li}{\CJKfamily{li}}%隶书

%\usepackage[bf]{package}
%\geometry{a4paper,scale=0.8}
\hypersetup{hidelinks}

\setcounter{secnumdepth}{0}
\begin{document}

EOD;
        $latex_data .= "\\lhead{".$left_head_title."} \n";
        $latex_data .= "\\chead{".$middle_head_title."} \n";
        $latex_data .= "\\rhead{".$right_head_title."} \n";
        $latex_data .=  <<<'EOD'
    \begin{center}
        \li\yihao 翻墙者
        \\\li\xiaoer 明日头条
    \end{center}
    \begin{center}
        \textbf{使用说明：}
        \begin{itemize}
            \item 本文中可通过点击新闻全文跳转至新闻页面。
            \item 本文由《翻墙者》杂志分部“明日头条”编写
            \item 联系我们可以通过如下方式：fqzofficial@protonmail.com
        \end{itemize}		
    \end{center}
    \newpage
    \tableofcontents
    \newpage
EOD;
        try{
            $articles = $data['articles'];
            foreach($articles as $article){
                $sequence = array_key_exists('sequence', $article) ? $article['sequence'] : 0;
                $title = array_key_exists('title', $article) ? $article['title'] : '';
                $link = array_key_exists('link', $article) ? $article['link'] : '';

                $link = preg_replace("/[\x{4e00}-\x{9fa5}]+/u", '', $link);

                // Better use this (credits to Kobi's answer below)
                $link = preg_replace("/\p{Han}+/u", '', $link);

                $footnote = array_key_exists('footnote', $article) ? $article['footnote'] : '';
                $content = array_key_exists('content', $article) ? $article['content'] : [];


                $title = $this->formatLatexStr($title);
                $footnote = $this->formatLatexStr($footnote);
                $link = $this->formatLatexStr($link);

                $latex_data .= "\\subsection{" . $title . "} \n";
                $contentText = '';
                $isFirst = true;
                foreach($content as $paragraph){
                    $paragraph = $this->formatLatexStr($paragraph);
                    if($isFirst){
                        $contentText .= "$paragraph\n";
                        $isFirst = false;
                    } else {
                        $contentText .= "\\\\\\indent$paragraph\n";
                    }
                }
                $latex_data .= "\\href{" . $link . "}{" . $contentText . "} \n";
                $latex_data .= "\\footnote{" . $footnote . "} \n";

            }
            $latex_data .= "\n\\end{document}";
            array_map('unlink', glob( "latex.*"));
            $filename = 'latex.tex';
            $pdf_filename = 'latex.pdf';
            $file = fopen($filename, 'wb+');
            fwrite($file, $latex_data);
            fclose($file);
            $output1 = shell_exec("/usr/bin/xelatex latex.tex");
            $output2 = shell_exec("/usr/bin/xelatex latex.tex");

            $pdf_file= public_path(). "/$pdf_filename";
            $headers = array(
                'Content-Type: application/pdf'
            );
            #return response()->json([$output2,$output1,$output3],200);
            return response()->download($pdf_file,$pdf_filename,$headers);
        }catch(Exception $e){
            print_r($e);
        }

//        return response()->json($latex_data,200);
    }
}
